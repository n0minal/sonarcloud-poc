# Get modified files
modified_files=$(git diff-tree --name-only --no-commit-id -r $CI_MERGE_REQUEST_TARGET_BRANCH_SHA)

echo "Modified files: $modified_files"
